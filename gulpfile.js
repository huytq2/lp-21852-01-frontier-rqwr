var gulp = require('gulp'),
	sass = require('gulp-ruby-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	filter = require('gulp-filter'),
	browserSync = require('browser-sync').create(),
	reload = browserSync.reload,
	minifyCss = require('gulp-minify-css'),
	//imagemin	= require('gulp-imagemin'),
	//pngquant	= require('imagemin-pngquant'),
	replace = require('gulp-html-replace'),
	rename = require('gulp-rename');

gulp.task('sass', function() {
	return sass('landingpages-dev/**/*.scss', {sourcemap:true})
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('landingpages-dev'))
		.pipe(filter('**/*.css'))
		.pipe(reload({stream: true}));
});

gulp.task('serve', ['sass'], function() {
	browserSync.init({
		server:{
			baseDir: './',
			index: 'index.html'
		}
	});

	gulp.watch('landingpages-dev/**/*.scss', ['sass']);
	gulp.watch('landingpages-dev/**/*.html').on('change', reload);
});

gulp.task('imagemin', ['move'], function() {
	return gulp.src('landingpages-release/**/images/*')
		.pipe(imagemin({
			progressive: true,
			use: [pngquant()]
		}))
		.pipe(gulp.dest('landingpages-release'));
});
gulp.task('move', ['sass'], function() {
	return gulp.src(['landingpages-dev/**/*', '!**/css{,/**}'])
		.pipe(gulp.dest('landingpages-release'));
})
gulp.task('minify', ['move'], function(){
	gulp.src('landingpages-dev/**/*.css')
		.pipe(minifyCss())
		.pipe(rename({suffix: ".min"}))
		.pipe(gulp.dest('landingpages-release'));
})
gulp.task('replacecss', ['move'], function(){
	gulp.src(['landingpages-release/**/index.html'])
		.pipe(replace({
			css:{
				src:['css/main.min.css','css/normalize.min.css'],
				tpl: '<link rel="stylesheet" type="text/css" href="%s">'
			}
		}))
		.pipe(gulp.dest('landingpages-release'));
})


gulp.task('default', ['serve']);



gulp.task('release', ['sass','move','minify','replacecss','imagemin']);
